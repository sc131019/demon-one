package com.inno.business;

import com.inno.model.po.Paper;

import java.util.List;

public interface PaperManage {

    int addPaper(Paper paper);

    int deletePaper(long id);

    int updatePaper(Paper paper);

    Paper queryById(long id);

    List<Paper> queryAllPaper();
}
