package com.inno.business.impl;

import com.inno.business.PaperManage;
import com.inno.dao.PaperDao;
import com.inno.model.po.Paper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PaperManageImpl implements PaperManage {

    @Resource
    private PaperDao paperDao;


    @Override
    public int addPaper(Paper paper) {
        return paperDao.addPaper(paper);
    }

    @Override
    public int deletePaper(long id) {
        return paperDao.deletePaper(id);
    }

    @Override
    public int updatePaper(Paper paper) {
        return paperDao.updatePaper(paper);
    }

    @Override
    public Paper queryById(long id) {
        return paperDao.queryById(id);
    }

    @Override
    public List<Paper> queryAllPaper() {
        return paperDao.queryAllPaper();
    }
}
