package com.inno.dao;

import com.inno.model.po.Paper;

import java.util.List;

public interface PaperDao {

    int addPaper(Paper paper);

    int updatePaper(Paper paper);

    int deletePaper(long id);

    List<Paper> queryAllPaper();

    Paper queryById(long id);
}
