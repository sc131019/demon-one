package com.inno.model.po;


import lombok.Data;

@Data
public class Paper {

    private Integer id;
    private String name;
    private int number;
    private String detail;
}
