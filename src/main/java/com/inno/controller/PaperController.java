package com.inno.controller;

import com.inno.business.PaperManage;
import com.inno.model.po.Paper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/paper")
public class PaperController {

    @Resource
    private PaperManage paperManage;

    @RequestMapping("/add")
    public Object addPaper(Paper paper){
        return paperManage.addPaper(paper);
    }

    @RequestMapping("/update")
    public Object updatePaper(Paper paper){
        return paperManage.updatePaper(paper);
    }

    @RequestMapping("/delete")
    public Object deletePaper(long id){
        return paperManage.deletePaper(id);
    }

    @RequestMapping("/queryById")
    public Object queryById(long id){
        return paperManage.queryById(id);
    }

    @RequestMapping("/queryAll")
    public Object queryAll(){
        return paperManage.queryAllPaper();
    }
}
